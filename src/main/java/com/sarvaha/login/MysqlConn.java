package com.sarvaha.login;

import java.sql.*;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MysqlConn {
	// JDBC driver name and database URL

	
	Properties prop;
	public static Logger logger;
	public MysqlConn() {
		try 
		{
			prop = new Properties();
			prop.load(MysqlConn.class.getClassLoader().getResourceAsStream(
					"db.properties"));

			logger = LoggerFactory.getLogger(MysqlConn.class);
			

		} catch (Exception e) {
			logger.error("File Not Found");
		}

	}

	int isAuth(String uname, String pass) {
		Connection conn = null;
		Statement stmt = null;
		int flag = 0;

		String getDriver = prop.getProperty("JDBC_DRIVER");
		String getUrl = prop.getProperty("DB_URL");
		String getUser = prop.getProperty("USER");
		String getPass = prop.getProperty("PASS");
		

		try {
			
			// STEP 2: Register and load JDBC driver
			
			Class.forName(getDriver);
			// STEP 3: Open a connection
			logger.info("Connecting to database...");

			conn = DriverManager.getConnection(getUrl, getUser, getPass);

			// STEP 4: Execute a query
			logger.info("Creating statement...");
			stmt = conn.createStatement();
			String sql;
			sql = "SELECT username,password FROM user";

			// select id, username from usertabl where username ='';
			// select password from usertabl where username = ?

			ResultSet rs = stmt.executeQuery(sql);

			// STEP 5: Extract data from result set
			while (rs.next()) {
				if (uname.equalsIgnoreCase(rs.getString("username"))
						|| pass.equals(rs.getString("password"))) {

					if (uname.equalsIgnoreCase(rs.getString("username"))) {
						flag = 1;
					} else {
						flag = 2;
					}
				}

				// Retrieve by column name
				// int id = rs.getInt("id");
				if (uname.equalsIgnoreCase(rs.getString("username"))
						&& pass.equals(rs.getString("password"))) {
					flag = 3;

				}

				
			}
			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();
			logger.info("Clearing the ResultSet, closing Statement and closing the Connection");
		} 
		catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
		} catch (Exception e) {
			// Handle errors for Class.forName
			e.printStackTrace();
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
			}// nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}// end finally try
		}// end try
		
		return flag;
	}// end main
	
}// end FirstExample

