package com.sarvaha.login;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class MyAuth extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException 
	{
		
		//PrintWriter out=resp.getWriter();
		resp.setContentType("text/html");
		
		String name=req.getParameter("username");
		String pass=req.getParameter("password");
		
		MysqlConn mc=new MysqlConn();
		int flag=mc.isAuth(name, pass);
		
		req.setAttribute("flag", String.valueOf(flag));
		req.setAttribute("name",name);
		RequestDispatcher reqDispatcher=getServletConfig().getServletContext().getRequestDispatcher("/UserChecking.jsp"); 
		reqDispatcher.forward(req, resp);
	
	}

}
